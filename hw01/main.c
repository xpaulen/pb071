#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define O 63 // Out of range with bits

void print_error_message(char *message)
{
    fprintf(stderr, "%s\n", message);
}

int power(int a, int b)
{
    int result = 1;
    while(b != 0){
        result = result * a;
        b--;
    }
    return result;
}

int print_binary(uint64_t dec)
{
    int arr[65];
    int len = 0;
    if (!dec){
        printf("# 0\n");
        return 1;
    }
    while (dec) {
        if (dec & 1){
            arr[len] = 1;
            len++;
        }
        else{
            arr[len] = 0;
            len++;
        }
        dec = dec >> 1;
    }
    len--;
    printf("# ");
    while(len >= 0){
        printf("%d", arr[len]);
        len--;
    }
    printf("\n");
    return 1;
}

int to_dec(char *arr, int* index, int base)
{
    uint64_t result = 0;
    int start = 0;
    int curent = 0;
    int num = 0;
    *index = *index -1;
    while(*index != -1){
        num = *index;
        if (base == 16){
            switch(toupper(arr[start])){
                case 'A':
                case 'B':
                case 'C':
                case 'D':
                case 'E':
                case 'F':
                    result = result + ((10 + toupper(arr[start]) - 'A') * power(16, num));
                    break;
                default:
                    curent = arr[start] - '0';
                    result = result + (curent * power(16, num));
                    break;
            }
        }
        else{
            curent = arr[start] - '0';
            result = result + (curent * power(base, num));
        }
        *index = *index - 1;
        start++;
    }
    *index = 0;
    return result;
}

//toto bohuzial neviem ako som mal opravit bez toho aby som narobil viac spiny v kode
int calculate_print(char* operator, int* base, char *arr, int* index, uint64_t* acumulator, int* m, uint64_t memory)
{
    int num = 0;
    if (*operator == 'E'){
        return 1;
    }
    if(*index == 0 && *operator != 'E'){
        print_error_message("Syntax error");
        return 0;
    }
    if(*m && *base != 10){
        print_error_message("Syntax error");
        return 0;
    }
    if(*m){
        num = memory;
    }
    else{
        num = to_dec(arr,index,*base);
    }
    switch(*operator){
        case 'P':
            *acumulator = num;
            break;
        case '+':
            *acumulator = *acumulator + num;
            break;
        case '-':
            *acumulator = *acumulator - num;
            break;
        case '*':
            *acumulator = *acumulator * num;
            break;
        case '/':
            if (num == 0){
                print_error_message("Division by zero");
                return 0;
            }
            *acumulator = *acumulator / num;         
            break;
        case '%':
            if (num == 0){
                print_error_message("Division by zero");
                return 0;
            }
            *acumulator = *acumulator % num;
            break;
        case '<':
            if(num > O){
                print_error_message("Out of range");
            }
            *acumulator = *acumulator << num;
            break;
        case '>':
            if(num > O){
                print_error_message("Out of range");
            }
            *acumulator = *acumulator >> num;
            break;
    }
    if (*operator != 'E'){
        printf("# %lu\n", *acumulator);
    }
    if(*m && *index != 1){
        print_error_message("Syntax error");
        return 0;
    }
    *index = 0;
    *operator = 'E';
    *base = 10;
    *m = 0;
    return 1;

}

bool calculate(void)
{   
    const char acceptable[22] = {"0123456789ABCDEFabcdef"};
    int error = 1; //if error ocured the program stops
    int input;
    char arr[65];
    uint64_t acumulator = 0;
    int index = 0;
    char operator = 'E';
    int memory_active = 0;
    int com_active = 0;
    int base = 10;
    uint64_t memory = 0;
    int num_error = 0; //error if number isnt in selected base
    while(error){
        input = getchar();
        if(num_error){
            error = calculate_print(&operator,&base,arr,&index,&acumulator,&memory_active,memory);
            if(error){
                print_error_message("Syntax error");
                error = 0;
            }
            break;
        }
        if(input == EOF){
            error = calculate_print(&operator,&base,arr,&index,&acumulator,&memory_active,memory);
            break;
        }
        if (input == '\n' && com_active){
            error = calculate_print(&operator,&base,arr,&index,&acumulator,&memory_active,memory);
            com_active = 0;
        }
        if (com_active){
            continue;
        }
        if (isspace(input)){
            continue;
        }
        switch(input){
            case '=':
                error = calculate_print(&operator,&base,arr,&index,&acumulator,&memory_active,memory);
                if(error){
                    printf("# %lu\n", acumulator);
                }
                break;
            case 'N':
                acumulator = 0;
                printf("# %lu\n", acumulator);
                operator = 'E';
                break;
            case ';':
                com_active = 1;
                break;
            case 'P':
            case '+':
            case '-':
            case '*':
            case '/':
            case '<':
            case '>':
            case '%':
                error = calculate_print(&operator,&base,arr,&index,&acumulator,&memory_active,memory);
                operator = input;
                break;
            case 'O':
                if ((operator != 'E' && index != 0) || base != 10){
                    error = calculate_print(&operator,&base,arr,&index,&acumulator,&memory_active,memory);
                }
                if(error){
                    printf("# %lo\n", acumulator);
                }
                if(operator != 'E'){
                    base = 8;
                }
                break;
            case 'X':
                if ((operator != 'E' && index != 0) || base != 10){
                    error = calculate_print(&operator,&base,arr,&index,&acumulator,&memory_active,memory);
                }
                if(error){
                    printf("# %lX\n", acumulator);
                }
                if(operator != 'E'){
                    base = 16;
                }
                break;
            case 'T':
                if ((operator != 'E' && index != 0) || base != 10){
                    error = calculate_print(&operator,&base,arr,&index,&acumulator,&memory_active,memory);
                }
                if(error){
                    print_binary(acumulator);
                }
                if(operator != 'E'){
                    base = 2;
                }
                break;
            case 'M':
                error = calculate_print(&operator,&base,arr,&index,&acumulator,&memory_active,memory);
                memory = acumulator;
                break;
            case 'R':
                error = calculate_print(&operator,&base,arr,&index,&acumulator,&memory_active,memory);
                memory = 0;
                break;
            case 'm':
                if (operator == 'E'){
                    print_error_message("Syntax error");
                    error = 0;
                    break;
                }
                if(index != 0){
                    error = calculate_print(&operator,&base,arr,&index,&acumulator,&memory_active,memory);
                    if(error){
                        print_error_message("Syntax error");
                        error = 0;
                    }
                    break;
                }
                index = 1;
                memory_active = 1;
                error = calculate_print(&operator,&base,arr,&index,&acumulator,&memory_active,memory);
                memory_active = 0;
                break;
            case 'l':
                error = calculate_print(&operator,&base,arr,&index,&acumulator,&memory_active,memory);
                operator = input;
                break;
            case 'r':
                error = calculate_print(&operator,&base,arr,&index,&acumulator,&memory_active,memory);
                operator = input;
                break;
            default:
                if (operator == 'E'){
                    print_error_message("Syntax error");
                    error = 0;
                    break;
                }
                if(com_active){
                    break;
                }
                switch(base){
                    case 2:
                        if((input - '0') >= 2){
                            num_error = 1;
                            break;
                        }
                        break;
                    case 8:
                        if((input - '0') >= 8){
                            num_error = 1;
                            break;
                        }
                        break;
                    case 10:
                        if((input - '0') >= 10){
                            num_error = 1;
                            break;
                        }
                        break;
                    case 16:
                        if (strchr(acceptable, input) == NULL){
                            num_error = 1;
                            break;
                        }
                        break;
                        
                }
                if(!num_error){
                    arr[index] = input;
                    index++;
                }
                break;
        }
    }
    if (error == 0){
        return false;
    }
    return true;
}

int main(void)
{
    if (!calculate()) {
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}
