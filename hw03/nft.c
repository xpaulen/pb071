#include <stdlib.h>
#include <string.h>

#include "capture.h"

int main(int argc, char *argv[])
{
    if(argc != 5){
        fprintf(stderr, "%s\n", "Wrong number of arguments\n");
        return EXIT_FAILURE;
    }
    if((strcmp(argv[4], "longestflow") != 0) && (strcmp(argv[4], "flowstats") != 0)){
        fprintf(stderr, "%s\n", "Wrong flow mode\n");
        return EXIT_FAILURE;
    }

    uint8_t ip_from[4];
    uint8_t ip_to[4];
    uint8_t mask_from = 33;
    uint8_t mask_to = 33;
    int flow_exit = 0;

    sscanf(argv[2], "%hhu.%hhu.%hhu.%hhu/%hhu",
            &ip_from[0], &ip_from[1], &ip_from[2], &ip_from[3], &mask_from);
    sscanf(argv[3], "%hhu.%hhu.%hhu.%hhu/%hhu",
            &ip_to[0], &ip_to[1], &ip_to[2], &ip_to[3], &mask_to);

    if(mask_from > 32 || mask_to > 32){
        fprintf(stderr, "%s\n", "Wrong mask\n");
        return EXIT_FAILURE;     
    }

    struct capture_t *capture = malloc(sizeof(struct capture_t));
    if (capture == NULL) {
        fprintf(stderr, "%s\n", "Failed malloc\n");
        return EXIT_FAILURE;
    }
    if(load_capture(capture, argv[1]) == -1){
        free(capture);
        fprintf(stderr, "%s\n", "Failed capture\n");
        return EXIT_FAILURE;
    }

    struct capture_t *filtered = malloc(sizeof(struct capture_t));
    if (filtered == NULL) {
        destroy_capture(capture);
        free(capture);
        fprintf(stderr, "%s\n", "Failed malloc\n");
        return EXIT_FAILURE;
    }

    if (filter_from_mask(capture, filtered, ip_from, mask_from) == -1){
        destroy_capture(capture);
        free(capture);
        free(filtered);
        fprintf(stderr, "%s\n", "Failed filter\n");
        return EXIT_FAILURE;
    }
    
    struct capture_t *filtered2 = malloc(sizeof(struct capture_t));
    if (filtered2 == NULL) {
        destroy_capture(filtered);
        free(filtered);
        destroy_capture(capture);
        free(capture);
        fprintf(stderr, "%s\n", "Failed malloc\n");
        return EXIT_FAILURE;
    }

    if (filter_to_mask(filtered, filtered2, ip_to, mask_to) == -1){
        destroy_capture(filtered);
        free(filtered);
        destroy_capture(capture);
        free(capture);
        free(filtered2);
        fprintf(stderr, "%s\n", "Failed filter\n");
        return EXIT_FAILURE;
    }

    if(strcmp(argv[4], "longestflow") == 0){
        flow_exit = print_longest_flow(filtered2);
    }
    if(strcmp(argv[4], "flowstats") == 0){
        flow_exit = print_flow_stats(filtered2);
    }

    destroy_capture(capture);
    destroy_capture(filtered);
    destroy_capture(filtered2);
    free(capture);
    free(filtered);
    free(filtered2);

    if(flow_exit != 0){
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
