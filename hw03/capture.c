#include <stdlib.h>
#include <string.h>
#include "capture.h"

struct node_flow_t{
    uint8_t from[4];
    uint8_t to[4];
    int packet_count;
    uint32_t first_sec;
    uint32_t first_usec;
    uint32_t last_sec;
    uint32_t last_usec;
    uint32_t sec;
    uint32_t usec;
    struct node_flow_t* next;
};

struct filter_info_t{
    uint8_t source_ip[4];
    uint8_t destination_ip[4];
    uint8_t protocol;
    uint8_t size;
    uint8_t network_prefix[4];
    uint8_t mask_length;
};

/* TODO implementace funkcionality */
#define UNUSED(x) ((void) x)

int load_capture(struct capture_t *capture, const char *filename)
{
    capture->count = 0;
    capture->packets = 0;
    capture->first = NULL;
    int pcap_end = PCAP_SUCCESS;
    struct pcap_context context[1];
    if (init_context(context, filename) == PCAP_LOAD_ERROR) {
        return -1;
    }
    capture->header = malloc(sizeof(struct pcap_header_t));
    if (capture->header == NULL) {
        destroy_context(context);
        return -1;
    }
    if (load_header(context, capture->header) != PCAP_SUCCESS) {
        destroy_capture(capture);
        destroy_context(context);
        return -1;
    }
    struct node_t* node1 = malloc(sizeof(struct node_t));
    if (node1 == NULL) {
        destroy_context(context);
        destroy_capture(capture);
        return -1;
    }
    node1->next = NULL;
    node1->data = malloc(sizeof(struct packet_t));
    if (node1->data == NULL) {
        free(node1);
        destroy_context(context);
        destroy_capture(capture);
        return -1;
    }
    if ((pcap_end = load_packet(context, node1->data)) == PCAP_LOAD_ERROR) {
        free(node1);
        destroy_context(context);
        destroy_capture(capture);
        return -1;
    }
    if(pcap_end == PCAP_FILE_END){
        free(node1->data);
        free(node1);
        destroy_context(context);
        return 0;
    }
    capture->first = node1;
    capture->packets += 1;
    capture->count += node1->data->packet_header->orig_len;
    struct node_t* current = node1;
    while(pcap_end == PCAP_SUCCESS){
        struct node_t* node2 = malloc(sizeof(struct node_t));
        if (node2 == NULL) {
            free(node2);
            pcap_end = PCAP_LOAD_ERROR;
            break;
        }
        node2->data = malloc(sizeof(struct packet_t));
        if (node2->data == NULL) {
            free(node2->data);
            free(node2);
            pcap_end = PCAP_LOAD_ERROR;
            break;
        }
        pcap_end = load_packet(context, node2->data);
        if(pcap_end == PCAP_SUCCESS){
            current->next = node2;
            capture->count += node2->data->packet_header->orig_len;
            capture->packets += 1;
            current = node2;
            node2->next = NULL;
        }
        else{
            free(node2->data);
            free(node2);
        }
    }
    if (pcap_end == PCAP_LOAD_ERROR){
        destroy_capture(capture);
        destroy_context(context);
        return -1;
    }
    destroy_context(context);
    return 0;
}

void destroy_capture(struct capture_t *capture)
{
    struct node_t* current = capture->first;
    struct node_t* delete = NULL;
    while(current != NULL){
        delete = current;
        current = current->next;
        destroy_packet(delete->data);
        free(delete->data);
        free(delete);
    }
    free(capture->header);
}

const struct pcap_header_t *get_header(const struct capture_t *const capture)
{
    return capture->header;
}

struct packet_t *get_packet(
        const struct capture_t *const capture,
        size_t index)
{
    if(index > packet_count(capture)){
        return NULL;
    }
    struct node_t* current = capture->first;
    while(current != NULL && index != 0){
        current = current->next;
        index--;
    }
    if(index == 0 && current != NULL){
        return current->data;
    }
    return NULL;
}

size_t packet_count(const struct capture_t *const capture)
{
    return capture->packets;
}

size_t data_transfered(const struct capture_t *const capture)
{
    return capture->count;
}

int filter_all(
        const struct capture_t *const original,
        struct capture_t *filtered,
        struct filter_info_t info,
        bool (*if_fun) (struct node_t*, struct filter_info_t)
        )
{
    struct node_t* current = original->first;
    struct node_t* last = NULL;
    filtered->first = NULL;
    filtered->count = 0;
    filtered->packets = 0;
    filtered->header = malloc(sizeof(struct pcap_header_t));
    if(filtered->header == NULL){
        return -1;
    }
    memcpy(filtered->header, original->header, sizeof(struct pcap_header_t));
    while (current != NULL){
        if ((*if_fun)(current, info)){
            struct node_t* new = malloc(sizeof(struct node_t));
            if (new == NULL) {
                free(new);
                destroy_capture(filtered);
                return -1;
            }
            new->data = malloc(sizeof(struct packet_t));
            if (new->data == NULL) {
                free(new->data);
                free(new);
                destroy_capture(filtered);
                return -1;
            }
            copy_packet(current->data, new->data);
            new->next = NULL;
            filtered->count += new->data->packet_header->orig_len;
            filtered->packets += 1;
            if(filtered->first == NULL){
                filtered->first = new;
            }
            else{
                last->next = new;
            }
            last = new;
        }
        current = current->next;
    }
    return 0;
}

bool filter_protocol_if(struct node_t* current, struct filter_info_t info){
    return (current->data->ip_header->protocol == info.protocol);
}

int filter_protocol(
        const struct capture_t *const original,
        struct capture_t *filtered,
        uint8_t protocol)
{
    struct filter_info_t info;
    info.protocol = protocol;
    return filter_all(original, filtered, info, &filter_protocol_if);
}

bool filter_larger_if(struct node_t* current, struct filter_info_t info){
    return (current->data->packet_header->orig_len > info.size);
}

int filter_larger_than(
        const struct capture_t *const original,
        struct capture_t *filtered,
        uint32_t size)
{
    struct filter_info_t info;
    info.size = size;
    return filter_all(original, filtered, info, &filter_larger_if);
}

bool filter_from_to_if(struct node_t* current, struct filter_info_t info){
    return (current->data->ip_header->src_addr[0] == info.source_ip[0] &&
            current->data->ip_header->src_addr[1] == info.source_ip[1] &&
            current->data->ip_header->src_addr[2] == info.source_ip[2] &&
            current->data->ip_header->src_addr[3] == info.source_ip[3] &&
            current->data->ip_header->dst_addr[0] == info.destination_ip[0] &&
            current->data->ip_header->dst_addr[1] == info.destination_ip[1] &&
            current->data->ip_header->dst_addr[2] == info.destination_ip[2] &&
            current->data->ip_header->dst_addr[3] == info.destination_ip[3]);
}

int filter_from_to(
        const struct capture_t *const original,
        struct capture_t *filtered,
        uint8_t source_ip[4],
        uint8_t destination_ip[4])
{
    struct filter_info_t info;
    memcpy(info.source_ip, source_ip, 4*sizeof(uint8_t));
    memcpy(info.destination_ip, destination_ip, 4*sizeof(uint8_t));
    return filter_all(original, filtered, info, &filter_from_to_if);
}

bool filter_from_mask_if(struct node_t* current, struct filter_info_t info)
{
    if(info.mask_length == 0){
        return true;
    }
    uint32_t ip_mask = 0;
    uint32_t ip_curr = 0;
    ip_mask = info.network_prefix[0] << 24;
    ip_mask += info.network_prefix[1] << 16;
    ip_mask += info.network_prefix[2] << 8;
    ip_mask += info.network_prefix[3];
    ip_curr = current->data->ip_header->src_addr[0] << 24;
    ip_curr += current->data->ip_header->src_addr[1] << 16;
    ip_curr += current->data->ip_header->src_addr[2] << 8;
    ip_curr += current->data->ip_header->src_addr[3];
    return (ip_mask >> (32-info.mask_length) == ip_curr >> (32-info.mask_length));
}

int filter_from_mask(
        const struct capture_t *const original,
        struct capture_t *filtered,
        uint8_t network_prefix[4],
        uint8_t mask_length)
{
    struct filter_info_t info;
    info.mask_length = mask_length;
    memcpy(info.network_prefix, network_prefix, 4*sizeof(uint8_t));
    return filter_all(original, filtered, info, &filter_from_mask_if);
}

bool filter_to_mask_if(struct node_t* current, struct filter_info_t info)
{
    if(info.mask_length == 0){
        return true;
    }
    uint32_t ip_mask = 0;
    uint32_t ip_curr = 0;
    ip_mask = info.network_prefix[0] << 24;
    ip_mask += info.network_prefix[1] << 16;
    ip_mask += info.network_prefix[2] << 8;
    ip_mask += info.network_prefix[3];
    ip_curr = current->data->ip_header->dst_addr[0] << 24;
    ip_curr += current->data->ip_header->dst_addr[1] << 16;
    ip_curr += current->data->ip_header->dst_addr[2] << 8;
    ip_curr += current->data->ip_header->dst_addr[3];
    return (ip_mask >> (32 - info.mask_length) == ip_curr >> (32 - info.mask_length));
}

int filter_to_mask(
        const struct capture_t *const original,
        struct capture_t *filtered,
        uint8_t network_prefix[4],
        uint8_t mask_length)
{
    struct filter_info_t info;
    info.mask_length = mask_length;
    memcpy(info.network_prefix, network_prefix, 4*sizeof(uint8_t));
    return filter_all(original, filtered, info, &filter_to_mask_if);
}

void destroy_flow(struct node_flow_t* node)
{
    struct node_flow_t* delete = NULL;
    while(node != NULL){
        delete = node;
        node = node->next;
        free(delete);
    }
}

int init_node(struct node_flow_t* node, uint8_t ip_from[4], uint8_t ip_to[4], struct node_t* packet)
{
    node->packet_count = 1;
    memcpy(node->from, ip_from, 4 * sizeof(uint8_t));
    memcpy(node->to, ip_to, 4 * sizeof(uint8_t));
    node->first_sec = packet->data->packet_header->ts_sec;
    node->first_usec = packet->data->packet_header->ts_usec;
    node->last_sec = packet->data->packet_header->ts_sec;
    node->last_usec = packet->data->packet_header->ts_usec;
    node->next = NULL;
    return 0;
}

int cmp_all(struct node_flow_t* node, uint8_t ip_from[4], uint8_t ip_to[4], struct node_t* packet)
{
    if(node->next == NULL && node->packet_count == 0){
        init_node(node, ip_from, ip_to, packet);
        return 0;
    }
    struct node_flow_t* last = NULL;
    while(node != NULL){
        if(node->from[0] == ip_from[0] && node->from[1] == ip_from[1] &&
           node->from[2] == ip_from[2] && node->from[3] == ip_from[3] &&
           node->to[0] == ip_to[0] && node->to[1] == ip_to[1] &&
           node->to[2] == ip_to[2] && node->to[3] == ip_to[3]){
               node->packet_count += 1;
               node->last_sec = packet->data->packet_header->ts_sec;
               node->last_usec = packet->data->packet_header->ts_usec;
               return 0;
        }
        last = node;
        node = node->next;
    }
    struct node_flow_t* append = malloc(sizeof(struct node_flow_t));
    if(append == NULL){
        fprintf(stderr, "%s\n", "Failed malloc\n");
        return -1;
    }
    init_node(append, ip_from, ip_to, packet);
    last->next = append;
    return 0;
}

int print_flow_stats(const struct capture_t *const capture)
{
    if(capture->first == NULL){
        return 0;
    }
    struct node_t* current = capture->first;
    struct node_flow_t* root = malloc(sizeof(struct node_flow_t));
    if(root == NULL){
        fprintf(stderr, "%s\n", "Failed malloc\n");
        return -1;
    }
    root->next = NULL;
    root->packet_count = 0;
    while(current != NULL){
        if (cmp_all(root, current->data->ip_header->src_addr, 
            current->data->ip_header->dst_addr, current) != 0){
            destroy_flow(root);
            return -1;
        }
        current = current->next;
    }
    struct node_flow_t* current_flow = root;
    while(current_flow != NULL){
        printf("%u.%u.%u.%u -> ", current_flow->from[0], current_flow->from[1], current_flow->from[2], current_flow->from[3]);
        printf("%u.%u.%u.%u : ", current_flow->to[0], current_flow->to[1], current_flow->to[2], current_flow->to[3]);
        printf("%d\n", current_flow->packet_count);
        current_flow = current_flow->next;
    }
    destroy_flow(root);
    return 0;
}

struct node_flow_t* return_longest(struct node_flow_t* root){
    uint32_t max_sec = 0;
    uint32_t max_usec = 0;
    struct node_flow_t* result = NULL;
    while (root != NULL){
        root->sec = root->last_sec - root->first_sec;
        root->usec = root->last_usec - root->first_usec;
        if (max_sec == root->sec){
            if (max_usec < root->usec){
                max_sec = root->sec;
                max_usec = root->usec;
                result = root;
            }
        }
        if (max_sec < root->sec){
            max_sec = root->sec;
            max_usec = root->usec;
            result = root;
        }
        root = root->next;
    }
    return result;
}

int print_longest_flow(const struct capture_t *const capture)
{
    if(capture->first == NULL){
        fprintf(stderr, "%s\n", "Empty capture\n");
        return -1;
    }
    struct node_t* current = capture->first;
    struct node_flow_t* root = malloc(sizeof(struct node_flow_t));
    if(root == NULL){
        fprintf(stderr, "%s\n", "Failed malloc\n");
        return -1;
    }
    root->next = NULL;
    root->packet_count = 0;
    while(current != NULL){
        if (cmp_all(root, current->data->ip_header->src_addr, 
            current->data->ip_header->dst_addr, current) != 0){
            destroy_flow(root);
            return -1;
        }
        current = current->next;
    }
    struct node_flow_t* current_flow = return_longest(root);
    if(current_flow != NULL){
        printf("%u.%u.%u.%u -> ", current_flow->from[0], current_flow->from[1], current_flow->from[2], current_flow->from[3]);
        printf("%u.%u.%u.%u : ", current_flow->to[0], current_flow->to[1], current_flow->to[2], current_flow->to[3]);
        printf("%u:%u - ", current_flow->first_sec,current_flow->first_usec);
        printf("%u:%u\n", current_flow->last_sec,current_flow->last_usec);
    }
    destroy_flow(root);
    return 0;
}
