#include "sample_printer.h"

#include <stdlib.h>
#include <stdio.h>

#include <sys/types.h>
#include <dirent.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <pwd.h>
#include <grp.h>
#include <libgen.h>

#define UNUSED(x) ((void) x)

struct node{
    char* name;
    char* path;
    struct stat st;
};

struct array_node{
    int len;
    int index;
    struct node** arr;
};

int read_dir(char *directory);
int read_directory(char *directory, FILE* file, char* original_dir);
int check_file(char* directory, char* d_name, struct array_node* array, struct array_node* array_direct);
int print_stats(struct node* node, FILE* out, char* directory);
int arr_insert(struct array_node* array, char* name, char* path, struct stat stats);
void free_node(struct node* node);
void bub_sort(struct array_node* array);
int read_file(FILE* file, char* directory);
int set_perms(char* parameters[8], char* directory);
void destroy_par(char* parameters[8]);
void free_array(struct array_node* array);

int main(int argc, char** argv) {
    int i = 0;
    int e = 0;
    int opt = 0;

    if(argc > 4 || argc < 3){
        fprintf(stderr, "Incorrect number of args\n");
        return EXIT_FAILURE;
    }
    FILE* permissions_file = NULL;

    while((opt = getopt(argc, argv, ":i:e:")) != -1) { 
        switch(opt) { 
            case 'i':
                i = 1;
                permissions_file = fopen(optarg, "r");
                if (!permissions_file) {
                    perror("fopen");
                    return EXIT_FAILURE;
                }
                break;
            case 'e':
                e = 1;
                permissions_file = fopen(optarg, "w");
                if (!permissions_file) {
                    perror("fopen");
                    return EXIT_FAILURE;
                }
                break;
            case '?':
                fprintf(stderr, "Unknown option");
                printf("Try only -e or -i as option\n");
                return EXIT_FAILURE;
        }
    }

    char* path = NULL;
    char buff[1024];
    if (argc == 3){
        if(getcwd(buff, sizeof(char) * 1024) == NULL){
            fclose(permissions_file);
            perror("getcwd");
            return -1;
        }
        path = buff;
    }
    else{
        path = argv[3];
    }

    int error = 0;

    if(e)
        error = read_directory(path, permissions_file, path);

    if(i)
        error = read_file(permissions_file, path);

    fclose(permissions_file);

    return error;
}

int read_directory(char *directory, FILE* out, char* original_dir)
{
    struct dirent *entry;
    DIR *dir = opendir(directory);

    if (dir == NULL){
        perror("opendir");
        return -1;
    }

    struct stat stats;
    int file = open(directory, O_RDONLY);
    if(file == -1){
        closedir(dir);
        perror("open");
        return -1;
    }

    if (fstat(file, &stats) == -1){
        closedir(dir);
        close(file);
        perror("fstat");
        return -1;
    }

    struct node node;
    node.path = directory;
    node.st = stats;
    if(print_stats(&node, out, original_dir) == -1){
        closedir(dir);
        close(file);
        return -1;
    }
    
    close(file);

    struct array_node array;
    array.len = 64;
    array.index = 0;
    array.arr = malloc(sizeof(struct node*) * array.len);
    if(array.arr == NULL){
        closedir(dir);
        perror("malloc");
        return -1;
    }

    struct array_node array_direct;
    array_direct.len = 64;
    array_direct.index = 0;
    array_direct.arr = malloc(sizeof(struct node*) * array_direct.len);
    if(array_direct.arr == NULL){
        closedir(dir);
        perror("malloc");
        return -1;
    }

    while ((entry = readdir(dir)) != NULL)
    {
        if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0)
        {
            if (check_file(directory, entry->d_name, &array, &array_direct) == -1){
                closedir(dir);
                free_array(&array);
                free_array(&array_direct);
                return -1;
            }
        }
    }

    bub_sort(&array);
    bub_sort(&array_direct);

    for(int i = 0; i < array_direct.index; i++){
        if (read_directory(array_direct.arr[i]->path, out, original_dir) == -1){
            closedir(dir);
            free_array(&array);
            free_array(&array_direct);
            return -1;
        }
    }

    free_array(&array_direct);

    for(int i = 0; i < array.index; i++){
        if(print_stats(array.arr[i], out, original_dir) == -1){
            closedir(dir);
            free_array(&array);
            return -1;
        }
    }

    free_array(&array);
    closedir(dir);
    return 0;
}

int check_file(char* directory, char* d_name, struct array_node* array, struct array_node* array_direct)
{
    char* path = malloc((sizeof(char) * strlen(directory)) + (sizeof(char) * strlen(d_name)) + 2);
    if(path == NULL){
        perror("malloc");
        return -1;
    }
    strcpy(path, directory);
    strcat(path, "/");
    strcat(path, d_name);

    struct stat stats;
    int file = open(path, O_RDONLY);

    if(file == -1){
        perror("open");
        free(path);
        return -1;
    }

    if (fstat(file, &stats) == -1){
        free(path);
        close(file);
        perror("fstat");
        return -1;
    }
    close(file);

    if (S_ISREG(stats.st_mode))
        if(arr_insert(array, d_name, path, stats) == -1){
            free(path);
            return -1;
        }

    if(S_ISDIR(stats.st_mode))
        if(arr_insert(array_direct, d_name, path, stats) == -1){
            free(path);
            return -1;
        }
    return 0;
}

int print_stats(struct node* node, FILE* out, char* directory)
{
    char* path = node->path;
    struct stat stats = node->st;

    struct passwd* usr = getpwuid(stats.st_uid);
    if(usr == NULL){
        perror("getpwuid");
        return -1;
    }
    struct group* grp = getgrgid(stats.st_gid);
    if(usr == NULL){
        perror("getgrgid");
        return -1;
    }

    if(strlen(path) != strlen(directory))
        fprintf(out, "# file: %s\n", path + strlen(directory) + 1);
    else{
        fprintf(out, "# file: .\n");
    }

    fprintf(out, "# owner: %s\n", usr->pw_name);
    fprintf(out, "# group: %s\n", grp->gr_name);

    if ( (stats.st_mode & S_ISGID) || (stats.st_mode & S_ISUID) || (stats.st_mode & 01000)){
        fprintf(out, "# flags: ");
        fprintf(out, (stats.st_mode & S_ISUID) ? "s" : "-");
        fprintf(out, (stats.st_mode & S_ISGID) ? "s" : "-");
        fprintf(out, (stats.st_mode & 01000) ? "t" : "-");
        fprintf(out, "\n");
    }

    fprintf(out, "user::");
    fprintf(out, (stats.st_mode & S_IRUSR) ? "r" : "-");
    fprintf(out, (stats.st_mode & S_IWUSR) ? "w" : "-");
    fprintf(out, (stats.st_mode & S_IXUSR) ? "x" : "-");
    fprintf(out, "\ngroup::");
    fprintf(out, (stats.st_mode & S_IRGRP) ? "r" : "-");
    fprintf(out, (stats.st_mode & S_IWGRP) ? "w" : "-");
    fprintf(out, (stats.st_mode & S_IXGRP) ? "x" : "-");
    fprintf(out, "\nother::");
    fprintf(out, (stats.st_mode & S_IROTH) ? "r" : "-");
    fprintf(out, (stats.st_mode & S_IWOTH) ? "w" : "-");
    fprintf(out, (stats.st_mode & S_IXOTH) ? "x" : "-");
    fprintf(out, "\n\n");
    return 0;
}

int arr_insert(struct array_node* array, char* name, char* path, struct stat stats)
{
    struct node* node = malloc(sizeof(struct node));
    if(node == NULL){
        perror("malloc");
        return -1;
    }
    node->name = name;
    node->path = path;
    node->st = stats;
    if(array->index == array->len){
        array->len *= 2;
        if (realloc(array->arr, sizeof(struct node*) * array->len) == NULL){
            perror("realloc");
            return -1;
        }
    }
    array->arr[array->index] = node;
    array->index++;
    return 0;
}

void free_node(struct node* node)
{
    free(node->path);
    free(node);
}

void bub_sort(struct array_node* array)
{
  for(int i=0; i<array->index; i++){
    for(int j=0; j<array->index-1-i; j++){
      if(strcmp(array->arr[j]->name, array->arr[j+1]->name) > 0){
        struct node* temp = array->arr[j];
        array->arr[j] = array->arr[j+1];
        array->arr[j+1] = temp;
      }
    }
  }
}

int read_file(FILE* file, char* directory)
{
    size_t len = 25;
    char* buffer = malloc(sizeof(char) * len);
    if(buffer == NULL){
        perror("malloc");
        return -1;
    }
    char* name = NULL;
    char* owner = NULL;
    char* group = NULL;
    char* flags = NULL;
    char* own = NULL;
    char* grp = NULL;
    char* otr = NULL;
    char* null = NULL;
    char* parameters[8] = {name, owner, group, flags, own, grp, otr, null};
    int index = 0;
    int format[8] = {8, 9, 9, 9, 6, 7, 7, 0};
    while(getline(&buffer, &len, file) != -1){
        if(strstr(buffer, "# flags:") == NULL && index == 3){
            parameters[index] = NULL;
            index++;
        }

        parameters[index] = malloc(sizeof(char) * strlen(buffer + format[index]) + 1);
        if(parameters[index] == NULL){
            destroy_par(parameters);
            free(buffer);
            perror("malloc");
            return -1;
        }
        strcpy(parameters[index], buffer + format[index]);
        char* end = strchr(parameters[index],'\n');
        *end = 0;

        index++;
        if(index == 8){
            index = 0;
            if (set_perms(parameters, directory) == -2){
                destroy_par(parameters);
                free(buffer);
                return -1;
            }
        }
    }
    free(buffer);
    return 0;
}

int set_perms(char* parameters[8], char* directory)
{
    char* path = malloc((sizeof(char) * strlen(directory)) + (sizeof(char) * strlen(parameters[0])) + 2);
    if(path == NULL){
        perror("malloc");
        return -2;
    }
    strcpy(path, directory);
    strcat(path, "/");
    strcat(path, parameters[0]);

    struct stat stats;
    int file = open(path, O_RDONLY);
    if(file == -1){
        free(path);
        perror("open");
        return -2;
    }

    if (fstat(file, &stats) == -1){
        close(file);
        free(path);
        perror("fstat");
        return -2;
    }

    struct passwd* usr = getpwuid(stats.st_uid);
    if(usr == NULL){
        close(file);
        free(path);
        perror("getpwuid");
        return -2;
    }

    struct group* grp = getgrgid(stats.st_gid);
    if(usr == NULL){
        close(file);
        free(path);
        perror("getgrgid");
        return -2;
    }

    if(strcmp(parameters[1], usr->pw_name) != 0){
        fprintf(stderr, "User of file %s is incorrect\n", basename(path));
        free(path);
        close(file);
        destroy_par(parameters);
        return -1;
    }

    if(strcmp(parameters[2], grp->gr_name) != 0){
        fprintf(stderr, "Group of file %s is incorrect\n", basename(path));
        free(path);
        close(file);
        destroy_par(parameters);
        return -1;
    }

    mode_t mode = 00000;

    if(parameters[3] != NULL){
        if(parameters[3][0] == 's'){
            mode = mode | S_ISUID;
        }
        if(parameters[3][1] == 's'){
            mode = mode | S_ISGID;
        }
        if(parameters[3][2] == 't'){
            mode = mode | 01000;
        }
    }

    if(parameters[4][0] == 'r'){
        mode = mode | S_IRUSR;
    }
    if(parameters[4][1] == 'w'){
        mode = mode | S_IWUSR;
    }
    if(parameters[4][2] == 'x'){
        mode = mode | S_IXUSR;
    }
    if(parameters[5][0] == 'r'){
        mode = mode | S_IRGRP;
    }
    if(parameters[5][1] == 'w'){
        mode = mode | S_IWGRP;
    }
    if(parameters[5][2] == 'x'){
        mode = mode | S_IXGRP;
    }
    if(parameters[6][0] == 'r'){
        mode = mode | S_IROTH;
    }
    if(parameters[6][1] == 'w'){
        mode = mode | S_IWOTH;
    }
    if(parameters[6][2] == 'x'){
        mode = mode | S_IXOTH;
    }

    if (fchmod(file, mode) == -1){
        perror("chmod");
        return -1;
    }
    free(path);
    close(file);
    destroy_par(parameters);
    return 0;
}

void destroy_par(char* parameters[8])
{
    for(int i = 0; i < 8; i++){
        free(parameters[i]);
    }
}

void free_array(struct array_node* array){
    for(int i = 0; i < array->index; i++){
        free_node(array->arr[i]);
    }
    free(array->arr);
}


