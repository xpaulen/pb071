#include <stdbool.h>
#include "minesweeper.h"
#include <ctype.h>
#include <stdio.h>
#include <string.h>

#define UNUSED(A) (void) (A)

// i am sorry that i am doing it like this but first i had code that didnt care about
// mine count until the tile was revealed so this was the quickest thing how i could use my
// old code whitout changing many core things

#define W 100 //constant wrong flag that is added to number of surrounding mines
#define X 110 //constant hiden that is added to number of surrounding mines
#define S 10 //constant swich for switching between wrong flaged and hiden
#define MAX 99 //max board size
#define MIN 3 //min board size

void reveal_floodfill(size_t rows, size_t cols, uint16_t board[rows][cols], size_t row, size_t col);
int reveal_cell(size_t rows, size_t cols, uint16_t board[rows][cols], size_t row, size_t col);


/* ************************************************************** *
 *                         HELP FUNCTIONS                         *
 * ************************************************************** */

bool is_flag(uint16_t cell)
{
    return cell == 'F' || (cell >= W + 0 && cell < W + 8) || cell == 'W';
}

bool is_mine(uint16_t cell)
{
    return cell == 'M' || cell == 'F' || cell == 'm';
}

bool is_revealed(uint16_t cell)
{
    return cell == '.' || (cell <= '8' && cell > '0') || cell == 'm';
}

int get_number(uint16_t cell)
{
    if (cell == 'M' || cell == 'F' || cell == 'm'){
        return 0;
    }
    if (cell <= '8' && cell >= '0'){
        return cell - '0';
    }
    if (cell >= W + 0 && cell <= W + 8){
        return cell - W;
    }
    if (cell >= X + 0 && cell <= X + 8){
        return cell - X;
    }
    return -1;
}

/* ************************************************************** *
 *                         INPUT FUNCTIONS                        *
 * ************************************************************** */

bool set_cell(uint16_t *cell, char val)
{
    if(cell == NULL){
        return false;
    }
    val = toupper(val);
    if (val <= '8' && val >= '0'){
        *cell = val;
        return true;
    }
    switch(val){
        case 'W':
        case 'M':
        case 'F':
        case 'X':
        case '.':
            *cell = val;
            break;
        default:
            return false;
    }
    return true;
}

int load_board(size_t rows, size_t cols, uint16_t board[rows][cols])
{   
    const char ok_input[15] = {"MX.FW012345678"};
    int input;
    size_t i = 0;
    while (i != rows * cols){
        input = toupper(getchar());
        if(input == EOF){
            return -1;
        }
        if(strchr(ok_input, input) == NULL){
            continue;
        }
        *((uint16_t*) board + i) = input;
        i++;

    }
    return postprocess(rows, cols, board);
}

int surounding_mines(size_t i, size_t j, size_t rows, size_t cols, uint16_t board[rows][cols]){
    int mines = 0;
    for(short int x = -1; x < 2; x++){
        for(short int y = -1; y < 2; y++){
            if((int)i + x < 0 || (int)i + x > (int)rows - 1 || (int)j + y < 0 || (int)j + y > (int)cols - 1){
                continue;
            }
            if(is_mine(board[i + x][j + y])){
                mines++;
            }
        }
    }
    return mines;
}

int postprocess(size_t rows, size_t cols, uint16_t board[rows][cols])
{   
    int mines = 0;
    if(rows < MIN || rows > MAX || cols < MIN || cols > MAX){
        return -1;
    }
    for(size_t i = 0; i < rows; i++){
        for(size_t j = 0; j < cols; j++){
            if((i == 0 && j == 0) || (i == 0 && j == cols -1) || (i == rows -1 && j == 0) || (i == rows -1 && j == cols - 1)){
                if(board[i][j] == 'M' || board[i][j] == 'F'){
                    return -1;
                }
            }
            switch(board[i][j]){
                case 'M':
                case 'F':
                    mines++;
                    break;
                case 'X':
                    board[i][j] = surounding_mines(i, j, rows, cols, board) + X;
                    break;
                case 'W':
                    board[i][j] = surounding_mines(i, j, rows, cols, board) + W;
                    break;
                case '.':
                    board[i][j] = (surounding_mines(i, j, rows, cols, board) + '0');
                    break;
                default:
                    if (board[i][j] <= '8' && board[i][j] >= '0'){
                        if(get_number(board[i][j]) != surounding_mines(i, j, rows, cols, board)){
                            return -1;
                        }
                    }
                    else{
                        return -1;
                    }
                    break;
            }
        }
    }
    if(mines == 0){
        return -1;
    }
    return mines;
}

/* ************************************************************** *
 *                        OUTPUT FUNCTIONS                        *
 * ************************************************************** */

int print_board(size_t rows, size_t cols, uint16_t board[rows][cols])
{
    printf("   ");
    for(size_t i = 0; i < cols; i++){
        printf(" ");
        if (i < 10){
            printf(" %ld ", i);
        }
        else{
            printf("%ld ", i);
        }
    }
    printf("\n");
    for(size_t i = 0; i < rows; i++){
        printf("   ");
        for(size_t j = 0; j < cols; j++){
            printf("+---");
        }
        printf("+\n");
        if (i < 10){
            printf(" %ld ", i);
        }
        else{
            printf("%ld ", i);
        }
        for(size_t j = 0; j < cols; j++){
            switch(show_cell(board[i][j])){
                case 'F':
                    printf("|_F_");
                    break;
                case 'X':
                    printf("|XXX");
                    break;
                default:
                    printf("| %c ", show_cell(board[i][j]));
                    break;
            }
        }
        printf("|\n");
    }
    printf("   ");
    for(size_t j = 0; j < cols; j++){
            printf("+---");
        }
    printf("+\n");
    return 0;
}

char show_cell(uint16_t cell)
{
    if(cell == 'm'){
        return 'M';
    }
    if(cell == 'M' || (cell >= X + 0 && cell <= X + 8)){
        return 'X';
    }
    if(cell == 'F' || (cell >= W + 0 && cell <= W + 8)){
        return 'F';
    }
    if(cell <= '8' && cell >= '0'){
        if (cell == '0'){
            return ' ';
        }
        return cell; 
    }
    return -1;
}

/* ************************************************************** *
 *                    GAME MECHANIC FUNCTIONS                     *
 * ************************************************************** */

int count_r_mines(size_t rows, size_t cols, uint16_t board[rows][cols])
{
    int mines = 0;
    for(unsigned int i = 0; i < rows; i++){
        for(unsigned int j = 0; j < cols; j++){
            switch(board[i][j]){
                case 'M':
                    mines++;
                    break;
                default:
                    if(is_flag(board[i][j])){
                        mines--;
                    }
                    break;
            }
        }
    }
    return mines;
}

int reveal_cell(size_t rows, size_t cols, uint16_t board[rows][cols], size_t row, size_t col)
{
    if (row >= rows || col >= cols){
        return -1;
    }
    int holder = reveal_single(&(board[row][col]));
    if(holder == 0){
        if(board[row][col] == '0'){
            reveal_floodfill(rows, cols, board, row, col);
        }
    }
    return holder;
}

int reveal_single(uint16_t *cell)
{
    if (cell == NULL){
        return -1;
    }
    switch(*cell){
        case 'F':
            break;
        case 'M':
            *cell = 'm'; //mine that blew up
            return 1;
        default:
            if(*cell <= '8' && *cell >= '0'){
                return -1;
            }                
            if(*cell <= W + 8 && *cell >= W + 0){
                return -1;
            }
            if(*cell <= X + 8 && *cell >= X + 0){
                *cell -= 62; // 62 because from hiden that is mine count + 110 to ascii numbers that are count + 48
                return 0;
            }
        }
    return -1;
}

void reveal_floodfill(size_t rows, size_t cols, uint16_t board[rows][cols], size_t row, size_t col)
{
    for(short int x = -1; x < 2; x++){
        for(short int y = -1; y < 2; y++){
            if((int)row + x < 0 || row + x > rows - 1 || (int)col + y < 0 || col + y > cols - 1){
                continue;
            }
            if(board[row + x][col + y] != 'F' && is_flag(board[row + x][col + y])){
                board[row + x][col + y] += S;
            }
            reveal_cell(rows, cols, board, row + x, col + y);
            }
        }
}


int flag_cell(size_t rows, size_t cols, uint16_t board[rows][cols], size_t row, size_t col)
{
    if(board[row][col] == 'M'){
        board[row][col] = 'F';
        return count_r_mines(rows, cols, board);
    }
    if(board[row][col] == 'F'){
        board[row][col] = 'M';
        return count_r_mines(rows, cols, board);
    }
    if(is_flag(board[row][col])){
        board[row][col] += S;
        return count_r_mines(rows, cols, board);
    }
    if(!is_revealed(board[row][col])){
        board[row][col] -= S;
        return count_r_mines(rows, cols, board);
    }
    return INT16_MIN;
}

bool is_solved(size_t rows, size_t cols, uint16_t board[rows][cols])
{   
    int unrevealed = 0;
    int wrongflags = 0;
    for(size_t i = 0; i < rows; i++){
        for(size_t j = 0; j < cols; j++){
            if(board[i][j] < 119 && board[i][j] > 109){
                unrevealed++;
            }
            if(board[i][j] < 109 && board[i][j] > 99){
                wrongflags++;
            }
        }
    }
    if(unrevealed == 0 && wrongflags == 0){
        return true;
    }
    return false;
}

/* ************************************************************** *
 *                         BONUS FUNCTIONS                        *
 * ************************************************************** */

int generate_random_board(size_t rows, size_t cols, uint16_t board[rows][cols], size_t mines)
{
    // TODO: Implement me
    UNUSED(mines);

    // The postprocess function should be called at the end of the generate random board function
    return postprocess(rows, cols, board);
}

int find_mines(size_t rows, size_t cols, uint16_t board[rows][cols])
{
    // TODO: Implement me
    UNUSED(rows);
    UNUSED(cols);
    UNUSED(board);
    return -1;
}
