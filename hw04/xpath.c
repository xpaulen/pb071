#include "xpath.h"
#include "parser.h"
#include "vector.h"

void print_tree(struct node* node, FILE* outputfile){
    fprintf(outputfile, "<%s", node->name);
    if(node->keys != NULL){
        for (unsigned int i = 0;i < vec_size((node->keys)); i++){
            char **key = vec_get((node->keys), i);
            char **value = vec_get((node->values), i);
            fprintf(outputfile, " %s= \"%s\" ", *key, *value);
        }
    }
    fprintf(outputfile, ">");
    if(node->text != NULL){
        fprintf(outputfile, " %s ", node->text);
    }
    if(node->children != NULL){
        fprintf(outputfile, "\n");
        for (unsigned int i = 0;i < vec_size((node->children)); i++){
            struct node **child = vec_get((node->children), i);
            print_tree((*child), outputfile);
        }
    }
    fprintf(outputfile, "</%s>\n", node->name);
}

void print_text(struct node* node, FILE *outputfile){
    if(node->text != NULL){
        fprintf(outputfile, "%s\n", node->text);
    }
    if(node->children != NULL){
        for (unsigned int i = 0;i < vec_size((node->children)); i++){
            struct node **child = vec_get((node->children), i);
            print_text((*child), outputfile);
        }
    }
}

int check_node(struct node* node, int index, char** list, int len, struct node_t format, unsigned int pos, struct vector* to_print){
    int check = check_format(list[index], pos, node);
    if(check == 0){
        int check_child = check_children(node, index + 1, list, len, format, to_print);
        if (check_child == -1){
            vec_push_back(to_print, &node);
        }
        if(check_child == -2){
            return -1;
        }
    }
    if(check == -2){
        return -1;
    }
    return 0;
}

int check_children(struct node* node, int index, char** list, int len, struct node_t format, struct vector* to_print){
    if (index >= len){
        return -1;
    }
    for (unsigned int i = 0;i < vec_size((node->children)); i++){
        struct node **child = vec_get((node->children), i);
        if(check_node((*child), index, list, len, format, i+1, to_print) == -1){
            return -2;
        }
    }

    return 0;
}

int check_format(char* string, unsigned int position, struct node* node){
    char *cpy = malloc(sizeof(char) * (strlen(string) + 1));
    if(cpy == NULL){
        fprintf(stderr, "%s\n", "failed alloc\n");
        return -2;
    }
    strcpy(cpy, string);
    char *name = NULL;
    char *key = NULL;
    char *value = NULL;
    unsigned int pos = 0;
    int succ = 0;
    int error = 0;
    switch(mode_format(cpy)){
        case 'v':
            error = cmp_value(cpy, &name, &key, &value);
            break;
        case 'a':
            error = cmp_atribute(cpy, &name, &key);
            break;
        case 'p':
            error = cmp_position(cpy, &name, &pos);
            break;
        case 'n':
            name = cpy;
            break;
    }
    if(error == -1){
        free(cpy);
        fprintf(stderr, "%s\n", "wrong query\n");
        return -2;
    }

    if(strcmp(node->name, name) != 0){
        free(cpy);
        return -1;
    }

    if(key != NULL && value != NULL){
        for (unsigned int i = 0;i < vec_size((node->keys)); i++){
            char **key2 = vec_get((node->keys), i);
            char **value2 = vec_get((node->values), i);
            if(strcmp(*key2, key) == 0 && strcmp(value, *value2) == 0){
                succ = 1;
                break;
            }
        }
        if(succ != 1){
            free(cpy);
            return -1;
        }
    }

    if(key != NULL && value == NULL){
        for (unsigned int i = 0;i < vec_size((node->keys)); i++){
            char **key2 = vec_get((node->keys), i);
            if(strcmp(*key2, key) == 0){
                succ = 1;
                break;
            }
        }
        if(succ != 1){
            free(cpy);
            return -1;
        }
    }

    if(pos != 0 && position != pos){
        free(cpy);
        return -1;
    }

    free(cpy);
    return 0;

    
}

char mode_format(char* string){
    if(strchr(string, '=') != NULL){
        return 'v';
    }
    if(strchr(string, '@') != NULL){
        return 'a';
    }
    if(strchr(string, '[') != NULL){
        return 'p';
    }
    return 'n';
}

int cmp_position(char* string, char** name, unsigned int* pos){
    *name = string;
    string = strchr(string, '[');
    *string = 0;
    string += 1;
    char* position = string;
    string = strchr(string, ']');
    if(string == NULL){
        return -1;
    }
    *string = 0;
    if (string[1] != 0){
        return -1;
    }

    *pos = atoi(position);

    if(*pos == 0){
        return -1;
    }

    return 0;
}

int cmp_atribute(char* string, char** name, char** key){
    *name = string;
    string = strchr(string, '[');
    *string = 0;
    string += 1;
    if(*string != '@'){
        return -1;
    }
    *string = 0;
    string += 1;
    *key = string;
    string = strchr(string, ']');
    if(string == NULL){
        return -1;
    }
    *string = 0;
    if (string[1] != 0){
        return -1;
    }
    return 0;
}

int cmp_value(char* string, char** name, char** key, char** value){
    *name = string;
    string = strchr(string, '[');
    if(string == NULL){
        return -1;
    }
    *string = 0;
    string += 1;
    if(*string != '@'){
        return -1;
    }
    *string = 0;
    string += 1;
    *key = string;
    string = strchr(string, '=');
    if(string == NULL){
        return -1;
    }
    *string = 0;
    string += 1;
    if(*string != '"'){
        return -1;
    }
    *string = 0;
    string += 1;
    *value = string;
    string = strchr(string, '"');
    if(string == NULL){
        return -1;
    }
    *string = 0;
    string += 1;
    if(*string != ']'){
        return -1;
    }
    *string = 0;
    if (string[1] != 0){
        return -1;
    }
    return 0;
}

int xpath_init(struct node* node, char** list, int len, struct node_t format){
    struct vector *to_print = vec_create(sizeof(struct node *));
    if (to_print == NULL) {
        return -1;
    }

    if (check_node(node, 0, list, len, format, 1, to_print) == -1){
        free(to_print->data);
        free(to_print);
        return -1;
    }

    if(to_print->size == 0){
        free(to_print->data);
        free(to_print);
        return 0;
    }

    if(to_print->size == 1){
        struct node **child = vec_get((to_print), 0);
        if(format.xml_text[1] == 'x'){
            print_tree(*child, format.outputfile);
        }
        else{
            print_text(*child, format.outputfile);
        }
    }
    else{
        if(format.xml_text[1] == 'x'){
            fprintf(format.outputfile, "<result>");

            for (unsigned int i = 0;i < vec_size((to_print)); i++){
                struct node **child = vec_get((to_print), i);
                print_tree((*child), format.outputfile);
            }

            fprintf(format.outputfile, "</result>");
        }
        else{
            for (unsigned int i = 0;i < vec_size((to_print)); i++){
                struct node **child = vec_get((to_print), i);
                print_text((*child), format.outputfile);
            }
        }
    }

    free(to_print->data);
    free(to_print);

    return 0;
}
