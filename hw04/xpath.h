#ifndef XPATH_H
#define XPATH_H
#include <stdio.h>
#include "xparser.h"
#include "parser.h"
#include "vector.h"

struct node_t {
    char *xml_text;
    FILE *outputfile;
};

void print_text(struct node* node, FILE* outputfile);
void print_tree(struct node* node, FILE* outputfile);
char query_mode(char* str);
int check_node(struct node* node, int index, char** list, int len, struct node_t format, unsigned int pos, struct vector* to_print);
int check_children(struct node* node, int index, char** list, int len, struct node_t format, struct vector* to_print);
int str_cmp(struct node* node, char* str);
int cmp_position(char* string, char** name, unsigned int* pos);
int check_format(char* string, unsigned int position, struct node* node);
char mode_format(char* string);
int cmp_atribute(char* string, char** name, char** key);
int cmp_value(char* string, char** name, char** key, char** value);
int xpath_init(struct node* node, char** list, int len, struct node_t format);

#endif /* XPATH_H */
