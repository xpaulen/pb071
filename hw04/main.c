#include <stdio.h>
#include "xpath.h"
#include "parser.h"
#include "vector.h"
#include <string.h>

char mode(char *arg){
    if (strcmp(arg,"-i") == 0 || strcmp(arg,"--input") == 0)
        return 'i';
    if (strcmp(arg,"-o") == 0 || strcmp(arg,"--output") == 0)
        return 'o';
    if (strcmp(arg,"-t") == 0 || strcmp(arg,"--text") == 0)
        return 't';
    if (strcmp(arg,"-x") == 0 || strcmp(arg,"--xml") == 0)
        return 'x';
    return 'q';
}


int main(int argc, char **argv)
{
    struct node_t format;
    char *xml_text = NULL;
    char *inputfile = NULL;
    char *outputfile = NULL;
    char *query = NULL;
    int error = 0;
    for (int i = 1; i < argc; i++){
        char m = mode(argv[i]);
        switch(m){
            case 'i':
                if (inputfile == NULL){
                    inputfile = argv[i+1];
                    i++;
                }
                else{
                    fprintf(stderr, "%s\n", "intuptfile is defined\n");
                    return -1;                    
                }
                break;
            case 'o':
                if (outputfile == NULL){
                    outputfile = argv[i+1];
                    i++;
                }
                else{
                    fprintf(stderr, "%s\n", "outputfile is defined\n");
                    return -1;
                }
                break;
            case 't':
                    if (xml_text == NULL)
                    xml_text = ".txt";
                else{
                    fprintf(stderr, "%s\n", "text or xml already defined\n");
                    return -1;
                }
                break;
            case 'x':
                    if (xml_text == NULL)
                    xml_text = ".xml";
                else{
                    fprintf(stderr, "%s\n", "text or xml already defined\n");
                    return -1;
                }
                break;
            case 'q':
                if (argc == 1 + i){
                    query = argv[i];
                }
                else{
                    fprintf(stderr, "%s\n", "undefined number of arguments\n");
                    return -1;
                }
                break;
        }

    }

    if(xml_text == NULL){
        xml_text = ".txt";
    }

    if(query == NULL){
        fprintf(stderr, "%s\n", "query not defined\n");
        return -1;
    }

    FILE *in_file;
    FILE *out_file;

    if(inputfile == NULL){
        in_file = stdin;
    }
    else{
        if ((in_file = fopen(inputfile, "r")) == NULL){
            fprintf(stderr, "%s\n", "file doesnt exist\n");
                return -1;
        }

    }
    if(outputfile == NULL){
        out_file = stdout;
    }
    else{
        if ((out_file = fopen(outputfile, "w")) == NULL){
            fprintf(stderr, "%s\n", "file doesnt exist\n");
                return -1;
        }

    }

    format.outputfile = out_file;
    format.xml_text = xml_text;

    struct node* root = parse_xml(in_file);

    if(root == NULL){
        goto end;
    }

    char *cpy = malloc(sizeof(char) * (strlen(query) + 1));
    if(cpy == NULL){
        fprintf(stderr, "%s\n", "failed alloc\n");
        error = -1;
        node_destroy(root);
        goto end;
    }
    strcpy(cpy, query);

    if(cpy[strlen(query) - 1] == '/'){
        fprintf(stderr, "%s\n", "wrong query\n");
        error = -1;
        free(cpy);
        node_destroy(root);
        goto end;
    }

	char *ptr = strtok(cpy, "/");
    
    int length = 30;
    int index = 0;

    char **list = malloc(sizeof(char *) * length);
    if (list == NULL){
        node_destroy(root);
        free(cpy);
        error = -1;
        fprintf(stderr, "%s\n", "failed alloc\n");
        goto end;
    }

    while(ptr != NULL){
        if(index == length){
            length *= 2;
            list = realloc(list ,sizeof(char *) * length);
        }
        char *str1 = malloc(sizeof(char) * (strlen(ptr)+1));
        if(str1 == NULL){
            error = -1;
            fprintf(stderr, "%s\n", "failed alloc\n");
            goto str_end;
        }
        list[index] = strcpy(str1, ptr);
        index++;
        ptr = strtok(NULL, "/");
    }

    error = xpath_init(root, list, index, format);

    str_end:
    
    node_destroy(root);

    while(index != 0){
        free(list[index - 1]);
        index--;
    }
    free(list);
    free(cpy);

    end:

    if (in_file != stdin)
        fclose(in_file);
    if (out_file != stdout)
        fclose(out_file);

    return error;
}